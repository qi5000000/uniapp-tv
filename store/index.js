import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		currentPage: null,
		currentZone: null
	},
	mutations: {
		switchZone(state, zone) {
			state.currentZone = zone
		},
		switchPage(state,page){
			state.currentPage=page;
		}
	},
	actions: {
		
	}
})

export default store
